<?php

namespace App\Controller;

use App\Entity\Serie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AnalysePageController extends AbstractController
{
    /**
     * @Route("/analyse/page", name="analyse_page")
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function index()
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://www.zone-annuaire.com/serie-vostfr/');
        $content = "";
        $content = $response->getContent();
        dump($content);
        $analyse = explode("\n",$content);
        $liste_image = array();
        foreach ($analyse as $line){
            if (strpos($line, "mainimg") !== false ){
                $serie = new Serie();
                $serie->setAffiche( explode('"',explode("src",$line)[1])[1]);
                $serie->setLien(explode('"',explode("href",$line)[1])[1]);
                $liste_image[] = $serie;
            }
        }

        return $this->render('analyse_page/index.html.twig', [
            'liste_series' => $liste_image,
        ]);
    }

}
